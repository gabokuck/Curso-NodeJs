// let getNombre = async() => {
//     return 'Gabriel';
// };

let getNombre = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Gabriel');
        }, 3000);
    });
}

let saludo = async() => {
    let nombre = await getNombre();
    return `Hola ${ nombre }`;
}


saludo().then(mensaje => {
    console.log(mensaje);
})