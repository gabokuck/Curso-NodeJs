const axios = require('axios')

const getClima = async(lat, lng) => {


    let resp = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&units=metric&appid=cb49bfd0c599d6a7912db80585bbd7e1`)

    return resp.data.main.temp;
}



module.exports = {
    getClima
}