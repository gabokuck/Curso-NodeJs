const colors = require('colors');


let textoAzul = (input) => {
    console.log('Texto color azul'.blue);
    return;
}

let textoRojo = (input) => {
    console.log('Texto color rojo'.red);
    return;
}

let textoAmarillo = (input) => {
    console.log('Texto color amarillo'.yellow);
    return;
}

let textoVerde = (input) => {
    console.log('Texto color verde'.green);
    return;
}

module.exports = {
    textoRojo,
    textoAzul,
    textoAmarillo,
    textoVerde
}