const argv = require('./config/yargs').argv;
const colors = require('colors');
const { textoRojo, textoAzul, textoAmarillo, textoVerde } = require('./colores/colores')

let input = argv._[0];

let texto = 'Este es un texto color';

switch (input) {
    case 'azul':
        textoAzul();
        break;
    case 'rojo':
        textoRojo();
        break;
    case 'amarillo':
        textoAmarillo();
        break;
    case 'verde':
        textoVerde();
        break;
    default:
        console.log('Comando no reconocido');
}