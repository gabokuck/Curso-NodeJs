const argv = require('yargs')
    .command('azul', 'Muestra en consola el color azul')
    .command('rojo', 'Muestra texto en color rojo')
    .command('amarillo', 'Muestra texto en color amarillo')
    .command('verde', 'Muestra texto en color verde')
    .help()
    .argv;

module.exports = {
    argv
}