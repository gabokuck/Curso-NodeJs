# App calculadora

## Comado para instalar 

```
npm install
```

## Ejemplo
```
node app sumar -u 10 -d 20
```

## Comado para sumar 

```
node app sumar 
```
- --numero1 -u 
- --numero2 -d

## Comado para restar

```
node app restar 
```
- --numero1 -u 
- --numero2 -d

## Comado para multiplicar

```
node app multiplicar
```
- --numero1 -u 
- --numero2 -d

## Comado para dividir

```
node app dividir
```
- --numero1 -u
- --numero2 -d

# Captura de pantalla
![calculadora imagen](https://gitlab.com/gabokuck/Curso-NodeJs/raw/master/proyectos-gabriel/Calculadora/Captura.PNG "Calculadora")
