const colors = require('colors');
const argv = require('./config/yargs').argv;
const { sumar, restar, multiplicar, dividir } = require('./calculadora/calculadora');


let comando = argv._[0];

switch(comando){
	case 'sumar':
	sumar(argv.numero1, argv.numero2);
	break;
	case 'restar':
	restar(argv.numero1, argv.numero2);
	break;
	case 'multiplicar':
	multiplicar(argv.numero1, argv.numero2);
	break;
	case 'dividir':
	dividir(argv.numero1, argv.numero2);
	break;
	default:
	console.log(`El comando ${comando.red} no existe!`)
}
