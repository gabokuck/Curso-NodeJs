const colors = require('colors');

const sumar = (numero1, numero2) =>{
	let resultado = numero1 + numero2;
	return console.log(`El resultado de la suma de ${colors.red(numero1)} + ${colors.red(numero2)} es : ${colors.green(resultado)}`);
}

const restar = (numero1, numero2) =>{
	let resultado = numero1 - numero2;
	return console.log(`El resultado de la resta de ${colors.red(numero1)} + ${colors.red(numero2)} es : ${colors.green(resultado)}`);
}

const multiplicar = (numero1, numero2) =>{
	let resultado = numero1 * numero2;
	return console.log(`El resultado de la multiplicación de ${colors.red(numero1)} + ${colors.red(numero2)} es : ${colors.green(resultado)}`);
}

const dividir = (numero1, numero2) =>{
	let resultado = numero1 / numero2;
	return console.log(`El resultado de la división de ${colors.red(numero1)} + ${colors.red(numero2)} es : ${colors.green(resultado)}`);
}

module.exports = {
	sumar,
	restar,
	multiplicar,
	dividir
}