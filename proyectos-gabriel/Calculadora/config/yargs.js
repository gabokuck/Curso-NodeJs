const opciones = {
	numero1: {
			demand: true,
			alias: 'u',
			desc: 'Introduce un número',
			type: 'number'
		},
		numero2: {
			demand: true,
			alias: 'd',
			desc: 'Introduce un número',
			type: 'number'
		}
}

const argv = require('yargs')
	.command('sumar', 'Suma dos números', opciones)
	.command('restar', 'Resta dos números', opciones)
	.command('multiplicar', 'Multiplica dos números', opciones)
	.command('dividir', 'Divide dos números', opciones)
	.help()
	.argv;


module.exports = {
	argv
}

